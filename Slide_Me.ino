
#include <Servo.h> //Servo library
#define echoPin 2 // attach pin D2 Arduino to pin Echo of HC-SR04
#define trigPin 3 //attach pin D3 Arduino to pin Trig of HC-SR04

Servo myservoL;
Servo myservoR;

int pos; 
int val;
unsigned long timer;
int count;
unsigned long num;
int temp;
bool flag;

void setup() 
{

  Serial.begin(9600);
  myservoL.attach(9);  // attaches the servo signal pin on pin D6
  myservoR.attach(10);// attaches the servo signal pin on pin D7
  flag=false;
  pos = 90; 
  count =0;
  num=0;
  timer=0;
  Serial.flush();

}

void loop() 
{

  if(Serial.available())
  {
   val = Serial.read(); 
  }
  else if ((flag==true)&&(timer<=num)){val='a';}
  else {val='b';}
     if (val == 'a')
     {  flag=true;      
        for (pos = 0; pos <= 180; pos += 1) 
        {
          // in steps of 1 degree
          myservoL.write(pos);
          myservoR.write(180-pos);     // tell servo to go to position in variable 'pos'
          delay(15);
          timer+=15;

        }
        for (pos = 180; pos >= 0; pos -= 1) 
        { 
          myservoR.write(180-pos);
          myservoL.write(pos);     // tell servo to go to position in variable 'pos'
          delay(15);
          timer+=15;// waits 15ms for the servo to reach the position
        }
      }     
 
     else if (val == 'b')
    {
      flag=false;
      while(pos!=90)
      {
      count =0;
      num=0;
      timer=0;
      Serial.flush();
       if(pos>90)
       {
       myservoL.write(180-pos); //Servo No Move 
       myservoR.write(pos); //Servo No Move 
        pos-=1;
        delay(15);
        }
       else
       {
       myservoL.write(pos); //Servo No Move 
       myservoR.write(180-pos); //Servo No Move 
       pos+=1;
       delay(15);
       }
      }  
    }   
    else if ((val>='0')&&(val<='9'))
    {
      temp=val-48;
      if (count ==0)
      {
      num+=temp*10*60;
      count+=1;
     }
     else if(count==1)
     {
      num+=temp*60;
      count+=1;
     }
     else if(count==2)
     {
      num+=temp*10;
      count+=1;
     }
     else if(count==3)
     {
      num+=temp;
      count+=1;
      num*=1000;
      flag=true;
     }
    }
}
